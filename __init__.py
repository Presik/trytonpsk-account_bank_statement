#The COPYRIGHT file at the top level of this repository contains the full
#copyright notices and license terms.

from trytond.pool import Pool
from . import statement
from . import account
from . import move
from . import bank
from . import configuration


def register():
    Pool.register(
        configuration.BankStatementConfiguration,
        account.Account,
        bank.Bank,
        bank.BankAccount,
        statement.BankStatement,
        statement.BankStatementLine,
        statement.BankStatementTransaction,
        move.Move,
        move.Line,
        statement.SearchBankUnreconciledsLinesStart,
        statement.CreateBankStatementStart,
        statement.BankReconciliation,
        bank.BankCodeRelation,
        module='account_bank_statement', type_='model')
    Pool.register(
        statement.UpdateBankStatementLines,
        statement.SearchBankUnreconciledLines,
        statement.CreateBankStatement,
        module='account_bank_statement', type_='wizard')
